
from datetime import datetime
from app import create_app, logger, redis, rabbitmq
from flask  import session

app = create_app()
logger.info("Starting webapp...")

redis.set("start_time", datetime.now().strftime("%Y-%m-%d %H:%M:%S"))

@app.route('/')
def hello_world():
    start_timed =redis.get("start_time")
    return f'Hello, World! start_time={start_timed}'

@app.route('/publish/<message>')
def publish(message):
    now = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    message = f"{now} hello"
    rabbitmq.publish(message)
    return ""

@app.route('/test-session/<key>', defaults={'message': None})
@app.route('/test-session/<key>/<message>')
def test_session(key, message):
    if message:
        session[key] = message
        return f"set session[{key}]={message}"
    else:
        return f"get sessions[{key}]={session[key]}" 
    

if __name__ == '__main__':
    app.run()
import redis
from flask_session import Session

class MySession(object):
    _instance = None
    def __init__(self, app=None):
        if app is not None:
            self.init_app(app)
            
   
    def init_app(self, app):
        # 建立 Redis 資料庫連線
        host = app.config["REDIS_HOST"]
        port = app.config["REDIS_PORT"]
        
        #print("app.secret_key:", app.secret_key)
        # Configure Redis for storing the session data on the server-side
        app.config['SESSION_TYPE'] = 'redis'
        app.config['SESSION_PERMANENT'] = False
        app.config['SESSION_USE_SIGNER'] = True
        app.config['SESSION_REDIS'] = redis.from_url(f'redis://{host}:{port}')

        # Create and initialize the Flask-Session object AFTER `app` has been configured
        server_session = Session(app)
        MySession._instance = server_session

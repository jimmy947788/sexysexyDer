from components.MyLogger import MyLogger
from components.MyRedis import MyRedis
from components.MyRabbitMQ import MyRabbitMQ
from components.MySession import MySession
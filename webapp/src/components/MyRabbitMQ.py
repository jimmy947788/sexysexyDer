 
import pika

class MyRabbitMQ(object):
    _instance = None
    def __init__(self, app=None):
        if app is not None:
            self.init_app(app)

    def publish(self, message):
        connection = MyRabbitMQ._instance 
        channel = connection.channel()
        channel.basic_publish(exchange='', routing_key='sexsexder', body=message)
            
    def init_app(self, app):
        # 建立 RabbitMQ 資料庫連線
        host = app.config["RABBITMQ_HOST"]
        port = app.config["RABBITMQ_PORT"]
        user = app.config["RABBITMQ_USERNAME"]
        passwd = app.config["RABBITMQ_PASSWORD"]
        print("host: %s, port: %s, user: %s, passwd: %s" % (host, port, user, passwd))
        credentials = pika.PlainCredentials(user, passwd)
        
        connection = pika.BlockingConnection(
            pika.ConnectionParameters(host=host, port=port, credentials=credentials))
        channel = connection.channel()
        channel.queue_declare(queue='sexsexder')
        MyRabbitMQ._instance = connection


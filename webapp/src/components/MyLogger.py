import os
import logging
from pathlib import Path
from datetime import datetime
from flask.logging import default_handler
 
class MyLogger(object):
    def __init__(self, app=None):
        if app is not None:
            self.init_app(app)

    def init_app(self, app):
        
         # 配置日志信息
        log_path = os.path.join(app.config["BASE_DIR"], 'logs')
        Path(log_path).mkdir(parents=True, exist_ok=True)
        
        # 日誌文件的保存路徑
        log_path = os.path.join(log_path, 'webapp-{:%Y-%m-%d}.log'.format(datetime.now()))
        
        # 創建handler(日誌處理器), 指定了輸出路徑
        file_handler = logging.FileHandler(log_path, encoding='UTF-8')
        stream_handler = logging.StreamHandler()
        
        # 設置日誌輸出格式
        formatter = logging.Formatter(
            fmt="%(asctime)s %(levelname)s %(filename)s %(funcName)s[line:%(lineno)d] %(message)s",
            datefmt="%Y-%m-%d %X"
        )
        # 爲handler(處理器)指定輸出的日誌格式
        file_handler.setFormatter(formatter)
        stream_handler.setFormatter(formatter)
        
        # 爲handler(處理器)指定終端輸出的日誌等級,即只有級別大於等於logging.WARNING的日誌纔會在終端輸出
        file_handler.setLevel(logging.DEBUG)
        if app.debug:
            stream_handler.setLevel(logging.DEBUG)
        else:
            stream_handler.setLevel(logging.WARNING)
       
        logger = logging.getLogger("webapp")
        logger.addHandler(file_handler)
        logger.addHandler(stream_handler)
    
        # 先移除flask日誌中默認的handler
        #app.logger.removeHandler(default_handler)
        app.logger.handlers.clear()
        # 爲此應用的日誌記錄器中添加一個handler(處理器)
        app.logger.addHandler(file_handler)
        app.logger.addHandler(stream_handler)
        
        logger_level = logging.getLevelName(app.config["LOGGER_LEVEL"]) 
        app.logger.setLevel(logger_level)
        logger.setLevel(logger_level)
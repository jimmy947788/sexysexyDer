from redis import Redis

class MyRedis(object):
    _instance = None
    def __init__(self, app=None):
        if app is not None:
            self.init_app(app)
            
    def set(self, key, value):
        MyRedis._instance.set(key, value)
    
    def get(self, key):
        return MyRedis._instance.get(key)

    def init_app(self, app):
        # 建立 Redis 資料庫連線
        host = app.config["REDIS_HOST"]
        port = app.config["REDIS_PORT"]
        MyRedis._instance = Redis(host=host, port=port, db=0, 
                                  decode_responses=True) # 預設取出的資料為 str
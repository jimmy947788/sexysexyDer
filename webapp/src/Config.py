import os
import pathlib
from dotenv import load_dotenv

basedir = os.path.abspath(os.path.dirname(__file__))  
 # flask 預設config
    # https://flask.palletsprojects.com/en/2.2.x/config/
class Config:
    #if not os.path.exists('.env'):
    #    env_dir = pathlib.Path(basedir).parent.parent.absolute()
    #    print(f"env_path={env_dir}")
    #    env_path = os.path.join(env_dir, 'env', 'dev.local.env')
    #    load_dotenv(env_path)
    #else:
    #load_dotenv()
    
    BASE_DIR = basedir
    LOGGER_LEVEL=os.environ.get('LOGGER_LEVEL', 'INFO')
    FLASK_RUN_HOST=os.environ.get('FLASK_RUN_HOST', 'localhost')
    FLASK_RUN_PORT=os.environ.get('FLASK_RUN_PORT', 5000)
    FLASK_ENV=os.environ.get('FLASK_ENV', 5000)

    # website
    SECRET_KEY=os.environ.get('SECRET_KEY') or 'hard to guess string' 
    
    # Mail Server                                                    
    MAIL_USERNAME = 'jimmy947788'
    MAIL_PASSWORD = 'P@ssw0rd'
    MAIL_SERVER = os.environ.get('MAIL_SERVER', 'smtp.googlemail.com')
    MAIL_PORT = int(os.environ.get('MAIL_PORT', '587'))
    MAIL_USE_TLS = os.environ.get('MAIL_USE_TLS', 'true').lower() in \
                                                    ['true', 'on', '1']
    SSL_REDIRECT = False
    
    # Database
    # SQLALCHEMY_RECORD_QUERIES = True
    # SQLALCHEMY_COMMIT_ON_TEARDOWN = True 
    SQLALCHEMY_DATABASE_URI= os.environ.get('SQLALCHEMY_DATABASE_URI') 
    #print(f"SQLALCHEMY_DATABASE_URI={SQLALCHEMY_DATABASE_URI}")
    
    #FLASKY_MAIL_SUBJECT_PREFIX = '[Flasky]'
    #FLASKY_MAIL_SENDER = 'Flasky Admin <flasky@example.com>'
    #FLASKY_ADMIN = os.environ.get('FLASKY_ADMIN')

    # 預設Admin帳號
    DEFAULE_USERNAME=os.environ.get('DEFAULE_USERNAME')
    DEFAULE_PASSWORD=os.environ.get('DEFAULE_PASSWORD')
    # redis 設定
    REDIS_HOST=os.environ.get('REDIS_HOST') 
    REDIS_PORT=int(os.environ.get('REDIS_PORT', "6379"))
    REDIS_PASSWORD=os.environ.get('REDIS_PASSWORD') 
    # rabbitmq 設定 
    RABBITMQ_HOST=os.environ.get('RABBITMQ_HOST') 
    RABBITMQ_PORT=int(os.environ.get('RABBITMQ_PORT', "5672"))
    RABBITMQ_USERNAME=os.environ.get('RABBITMQ_USERNAME') 
    RABBITMQ_PASSWORD=os.environ.get('RABBITMQ_PASSWORD') 
    #RABBITMQ_WEB_PORT=int(os.environ.get('RABBITMQ_WEB_PORT', "15672"))

    
    @staticmethod  
    def init_app(app):  
        pass
  
class DevelopmentConfig(Config):  
    #DEBUG = True  
    #TESTING = False
    #SQLALCHEMY_DATABASE_URI = os.environ.get('DEV_DATABASE_URI') or\
    #        'sqlite:///' + os.path.join(basedir,'data-dev.sqlite')  
    # 关闭数据库修改跟踪操作[提高性能]：
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    # 开启输出底层执行的sql语句
    SQLALCHEMY_ECHO = True
  
class TestingConfig(Config):  
    #DEBUG = True 
    #TESTING=True
    #SQLALCHEMY_DATABASE_URI = os.environ.get('TEST_DATABASE_URI') or\
    #        'sqlite:///' + os.path.join(basedir,'data-test.sqlite') 
    # 关闭数据库修改跟踪操作[提高性能]：
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    # 开启输出底层执行的sql语句
    SQLALCHEMY_ECHO = True
  
class ProductionConfig(Config):  
    #SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URI') or\
    #        'sqlite:///' + os.path.join(basedir,'data.sqlite')  
    # 关闭数据库修改跟踪操作[提高性能]：
    SQLALCHEMY_TRACK_MODIFICATIONS = True
    # 开启输出底层执行的sql语句
    SQLALCHEMY_ECHO = False
  
config={  
        'development':DevelopmentConfig,  
        'testing':TestingConfig,  
        'production':ProductionConfig,  
        'default':DevelopmentConfig  
    }  

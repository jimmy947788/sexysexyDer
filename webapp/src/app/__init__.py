
import importlib
import logging
from pathlib import Path
current_path = Path(__file__).parent.resolve()
print(f"current_path={current_path}")
import os
from flask import Flask
from flask_bootstrap import Bootstrap
from flask_mail import Mail
from flask_moment import Moment
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from components import MyLogger
from components import MySession
from components import MyRabbitMQ
from components import MyRedis
from flasgger import Swagger
from Config import config

basedir = os.path.abspath(os.path.dirname(__file__))  
bootstrap=Bootstrap()  
mail=Mail()  
moment=Moment()  
db=SQLAlchemy()       
migrate = Migrate() # this
mylogger = MyLogger()
logger = logging.getLogger("webapp")
redis = MyRedis()
rabbitmq = MyRabbitMQ()
mysession = MySession()
def create_app(env_name = "default"):
    
    worker_folder = Path(basedir).parent.absolute()
    #print(f"worker_folder={worker_folder}")
    template_folder=os.path.join(worker_folder, "templates")
    #print(f"template_folder={template_folder}")
    static_folder=os.path.join(worker_folder, "assets")
    #print(f"static_folder={static_folder}")
    
    app=Flask(__name__, 
              template_folder=template_folder, 
              static_folder=static_folder)
    config[env_name].init_app(app)
    app.config.from_object(config[env_name])

    mylogger.init_app(app)
    mysession.init_app(app)
    rabbitmq.init_app(app)
    redis.init_app(app)
    #MyDBHelper.init_app(app)
    
    swagger = Swagger(app) # 
    bootstrap.init_app(app)
    mail.init_app(app)
    moment.init_app(app)
    db.init_app(app)
    migrate.init_app(app, db)
  
    # TODO: 新增controller請在這裡註冊
    #controllers = importlib.import_module('controllers')
    #login_manager = controllers.login_manager
    #login_manager.init_app(app)
    
    #app.register_blueprint(controllers.authorizedBlueprint) 
    #app.register_blueprint(controllers.defaultBlueprint)
    #app.register_blueprint(controllers.clientBlueprint) 
    #app.register_blueprint(controllers.accountBlueprint)  
    #app.register_blueprint(controllers.explorerBlueprint) 
    
    #app.register_blueprint(controllers.systemBlueprint) 
    #app.register_blueprint(controllers.riskBlueprint)    
    #app.register_blueprint(controllers.processBlueprint) 
    
    return app  


#!/bin/bash

#export FLASK_APP=src/web.py
export FLASK_ENV=development
flask --env-file=../config/.env --app=src/web.py run